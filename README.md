# MCMTdataanalysis

Funzioni per l'analisi del Microcensimento mobilità e trasporti

Pacchetto con funzioni per ricreare tutti i valori necessari per la creazione delle tabelle e dei grafici così come presenti nel Rapporto dell'UST "Comportement de la population en matière de transports 2015", dalla sezione 2.1 alla 3.6.
Questo pacchetto srutta le funzioni del pacchetto "MCMTtools" che comprende quattro pacchetti:

MCMTdataanalysis
MCMTdataprep
MCMTdlcreation
MCMTdatapkg

Le funzioni interne ai primi due pacchetti necessitano come argometo "data" l'oggetto "MCMTdatapkg::Data_list": questo file di dati é composto dalle banche dati del MCMT includendo però solamente una parte delle variabili (quelle necessarie per ricreare la tabella A2 fornita dall'UST).
Questo oggetto di dati é utilizzato come argomento di "default" per le funzioni di analisi del pacchetto "MCMTdataanalysis" creando una dipendenza tra i pacchetti.
Perhcé le funzioni possano essere utilizzate é dunque necessario disporre anche del pacchetto "MCMTdatapkg": visto che questo pacchetto contiene i dati forniti dall'UST, non può essere fornito gratuitamente e non può essere condiviso in questo progetto. Le stesse osservazioni si applicano al pacchetto "MCMTtools" che contiene i primi tre pacchetti escluso il pacchetto "MCMTdatapkg".

Edit 10.09.2021: thanks to the input of @gibonet, I'm now aware that MCMTdataprep and MCMTdataanalysis packages can't be properly installed because of the internal dependency form the MCMTdatapkg package which can't be freely shared because of data protection reasons. I am currently developing a separate package that will not contain the original MRMT data and therefore will be shareable, so that users will be able to instal MCMTdataprep and MCMTdataanalysis packages.
